package net.neuralmetrics.eva.calc;

import net.neuralmetrics.eva.domain.aircraft.AircraftSchedule;
import net.neuralmetrics.eva.domain.employee.EmployeeSkill;

import java.util.List;

/**
 * This class serve as a singleton entry to update filters, valueRangeProviders with problem facts
 * Created by hoang on 16/07/2017.
 */

public class ProblemFactProvider {
    public static List<AircraftSchedule> aircraftSchedules;
    public static List<EmployeeSkill> employeeSkills;

    public static void setAircraftSchedules(List<AircraftSchedule> schedules)
    {
        aircraftSchedules=schedules;
    }
    public static void setEmployeeSkills(List<EmployeeSkill> mEmployeeSkills)
    {
        employeeSkills = mEmployeeSkills;
    }
}
