package net.neuralmetrics.eva.domain.aircraft;

import java.util.List;

/**
 * Created by hoang on 11/07/2017.
 */
public class Aircraft {
    private String name; // e.g. VN-A669

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
