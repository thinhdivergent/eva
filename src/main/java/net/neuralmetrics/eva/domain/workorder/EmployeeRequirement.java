package net.neuralmetrics.eva.domain.workorder;

import net.neuralmetrics.eva.domain.employee.EmployeeSkill;

/**
 * Created by hoang on 10/07/2017.
 */
public class EmployeeRequirement {
    private EmployeeSkill.SKILL_LIST skill;
    private int number;
    private WorkOrder workOrder;

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public WorkOrder getWorkOrder() {
        return workOrder;
    }

    public void setWorkOrder(WorkOrder workOrder) {
        this.workOrder = workOrder;
    }

    public EmployeeSkill.SKILL_LIST getSkill() {
        return skill;
    }

    public void setSkill(EmployeeSkill.SKILL_LIST skill) {
        this.skill = skill;
    }
}
