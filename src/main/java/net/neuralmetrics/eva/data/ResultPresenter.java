package net.neuralmetrics.eva.data;

import net.neuralmetrics.eva.calc.EvaSolution;
import net.neuralmetrics.eva.domain.workorder.WorkOrder;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * This beautifies the result and print it to the screen
 * Created by hoang on 17/07/2017.
 */
public class ResultPresenter {
    private EvaSolution solution;
    DateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

    public void setSolution(EvaSolution solution) {
        this.solution = solution;
    }

    public void showWorkOrders()
    {
        List<WorkOrder> list = solution.getWorkOrders();
        for(WorkOrder wo : list)
        {
            System.out.println(wo);
        }
    }

    public void showScore()
    {
        if (solution==null) return;
        System.out.println("HARD SCORE: "+solution.getScore().getHardScore()+", MED SCORE: " + solution.getScore().getMediumScore() +", SOFT SCORE: "+solution.getScore().getSoftScore());
    }

    public void showWorkOrderShift()
    {
        if (solution==null) return;
        List<WorkOrder> workOrders = solution.getWorkOrders();
        for(WorkOrder w : workOrders)
        {
            if (w.getNightShift()==null) System.out.println("AC: "+w.getAircraft().getName()+", DESC: "+w.getWorkDescription()+", NS: UNASSIGNED");
            else
            System.out.println("AC: "+w.getAircraft().getName()+", DESC: "+w.getWorkDescription()+", NS: "+df.format(w.getNightShift().getDate()));
        }
    }
}
